//
//  StartVC.m
//  RouteMock-ios
//
//  Created by Jose Catala on 28/11/2018.
//  Copyright © 2018 TrakGlobal Telematics. All rights reserved.
//

#import "StartVC.h"
#import "TrakCoreLocationEntity.h"
#import "TrakCoreMockRoute.h"
#import <MapKit/MapKit.h>

@interface StartVC () <MKMapViewDelegate, UITextFieldDelegate>

@property (nonatomic, strong) TrakCoreMockRoute *route;

@property (weak, nonatomic) IBOutlet MKMapView *myMap;
@property (strong, nonatomic) MKPointAnnotation *trackAnnotation;
@property (weak, nonatomic) IBOutlet UITextField *startAddress;
@property (weak, nonatomic) IBOutlet UITextField *finishAddress;
@property (strong, nonatomic) MKPolyline *polyline;
@property (strong, nonatomic) NSArray <TCoreLocationEntity*>* points;

@property (weak, nonatomic) IBOutlet UISlider *mySlider;
@property (weak, nonatomic) IBOutlet UISwitch *mySwitch;

@property double multiplyValue;

@end

@implementation StartVC

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    _multiplyValue = 1;
    _mySlider.value = 1;
    _mySlider.minimumValue = 1;
    _mySlider.maximumValue = 20;
    _polyline = [[MKPolyline alloc]init];
    _trackAnnotation = [[MKPointAnnotation alloc]init];
    _startAddress.text = @"Crewe";
    _finishAddress.text = @"Nantwitch";
}

#pragma mark - Actions

- (IBAction) btnRouteDidTap:(id)sender
{
    _route = nil;
    
    _route = [[TrakCoreMockRoute alloc]initFromLocation:_startAddress.text toLocation:_finishAddress.text];
    
    __weak __typeof(self) weakSelf = self;
    
    [_route routeHandler:^(NSArray * _Nonnull route) {
        weakSelf.points = [[NSArray alloc]initWithArray:route];
        [weakSelf showRoute:route];
    }];

}

- (IBAction)mySliderDidChange:(id)sender
{
    UISlider *slider = (UISlider *)sender;
    
    self.multiplyValue = slider.value;
}




#pragma mark - TextField delegate

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}

#pragma mark - Map delegate

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]]) return nil;
    
    static NSString *identifier = @"identifier";
    
    MKAnnotationView *view = [[MKAnnotationView alloc]initWithAnnotation:annotation reuseIdentifier:identifier];
    view.canShowCallout = NO;
    
    if ([annotation.title isEqualToString:@"tracker"])
    {
        view.image = [UIImage imageNamed:@"tracker"];
    }
    else if ([annotation.title isEqualToString:@"point"])
    {
        view.image = [UIImage imageNamed:@"red_point"];
    }
    else if ([annotation.title isEqualToString:@"gap"])
    {
        view.image = [UIImage imageNamed:@"blue_point"];
    }

    return view;
}

- (MKOverlayRenderer *) mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay
{
    MKPolylineRenderer* lineView = [[MKPolylineRenderer alloc] initWithPolyline:overlay];
    lineView.strokeColor = [UIColor blueColor];
    lineView.lineWidth = 7;
    return lineView;
}

#pragma mark - Map private methods

- (void) showRoute:(NSArray<TCoreLocationEntity*> *)route
{
    __block __typeof(self) blockSelf = self;
    
    [route enumerateObjectsUsingBlock:^(TCoreLocationEntity * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        @autoreleasepool {
            CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(obj.latitude, obj.longitude);
            MKPointAnnotation *point = [[MKPointAnnotation alloc]init];
            point.coordinate = coordinate;
            
            if (obj.gap)
            {
                point.title = @"gap";
            }
            else
            {
                point.title = @"point";
            }
            
            [blockSelf.myMap addAnnotation:point];
        }
    }];
    
    [blockSelf followTheRoute:route];
}

- (void) zoomToPolyLine:(MKMapView*)map polyline:(MKPolyline*)polyline animated:(BOOL)animated
{
    [map setVisibleMapRect:[polyline boundingMapRect] edgePadding:UIEdgeInsetsMake(30.0, 30.0, 30.0, 30.0) animated:animated];
}

- (void) zoomMapForTrackingToCoordinate:(CLLocationCoordinate2D)coordinate
{
    [self.myMap setRegion:MKCoordinateRegionMake(coordinate, MKCoordinateSpanMake(0.1f, 0.1f))animated:YES];
}

#pragma mark Simulate behaviour

- (void) followTheRoute:(NSArray <TCoreLocationEntity*>*)route
{
    TCoreLocationEntity *entity = [route firstObject];
    __block CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(entity.latitude, entity.longitude);
    self.trackAnnotation.coordinate = coordinate;
    self.trackAnnotation.title = @"tracker";
    [_myMap addAnnotation:_trackAnnotation];
    
    [self zoomMapForTrackingToCoordinate:coordinate];
    
    if (route.count > 2) [self moveToRouteIndex:1];
}

- (void) moveToRouteIndex:(NSInteger)idx
{
    TCoreLocationEntity *from = [_points objectAtIndex:idx];
    TCoreLocationEntity *to   = [_points objectAtIndex:idx + 1];
    
    NSTimeInterval interval = [from.timestamp timeIntervalSinceDate:to.timestamp];
    
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(to.latitude, to.longitude);
    
    MKMapCamera *camera = [[MKMapCamera alloc] init];
    camera.centerCoordinate = coordinate;
    camera.heading = from.course;
    camera.altitude = 600;

    __weak __typeof(self) weakSelf = self;
    
    [UIView animateWithDuration:(interval / _multiplyValue) delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        weakSelf.trackAnnotation.coordinate = coordinate;
        if (weakSelf.mySwitch.isOn)
        {
            weakSelf.myMap.camera = camera;
        }
    }completion:^(BOOL finished) {
        if (idx < (weakSelf.points.count - 2))
        {
            [self moveToRouteIndex:idx + 1];
        }
    }];
    
    //[_myMap setCenterCoordinate:coordinate animated:YES];
}

- (float) distanceFrom:(TCoreLocationEntity *)start and:(TCoreLocationEntity*)end
{
    CLLocation *startLocation = [[CLLocation alloc]initWithLatitude:start.latitude longitude:start.longitude];
    CLLocation   *endLocation = [[CLLocation alloc]initWithLatitude:end.latitude longitude:end.longitude];
    float distance = [startLocation distanceFromLocation:endLocation];
    
    return distance;
}
        
@end
