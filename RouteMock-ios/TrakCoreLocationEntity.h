//
//  TrakCoreLocationEntity.h
//  TrakCoreFramework
//
//  Created by Jose Catala on 02/11/2018.
//  Copyright © 2018 TrakGlobal Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CLLocation;

NS_ASSUME_NONNULL_BEGIN

@interface TCoreLocationEntity : NSObject <NSCoding, NSCopying>

@property (nonatomic) float latitude;
@property (nonatomic) float longitude;
@property (nonatomic) float altitude;
@property (nonatomic) float horizontalAccuracy;
@property (nonatomic) float verticalAccuracy;
@property (nonatomic) float speed;
@property (nonatomic) float course;
@property (nonatomic, copy) NSDate* timestamp;
@property (nonatomic) BOOL gap;

/*!
 *@note     If the location is nil will return nil, if you want to
 *          get an instance of the model use [init] instead
 */
- (instancetype) initWithLocation:(CLLocation * _Nullable)location;

@end

NS_ASSUME_NONNULL_END
