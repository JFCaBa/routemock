//
//  TrakMockRoute.m
//  RouteMock-ios
//
//  Created by Jose Catala on 04/12/2018.
//  Copyright © 2018 TrakGlobal Telematics. All rights reserved.
//

#import "TrakCoreMockRoute.h"
#import "TrakCoreLocationEntity.h"
#import "TrakCoreGCDTimer.h"
#import <MapKit/MapKit.h>

@interface TrakCoreMockRoute ()<TrakCoreGCDTimerDelegate>
{
    NSMutableArray <TCoreLocationEntity *>* points;
}

@property (strong, nonatomic) MKPolyline *polyline;
@property (strong, nonatomic) TrakCoreGCDTimer *timer;

@end

@implementation TrakCoreMockRoute

- (instancetype) initFromLocation:(nonnull NSString *)from toLocation:(nonnull NSString *)to
{
    self = [super init];
    
    if (self)
    {
        _timer = [[TrakCoreGCDTimer alloc]initWithInterval:1.0];
        _timer.delegate      = self;
        [self createRouteFrom:from to:to];
    }
    
    return self;
}


- (void) createRouteFrom:(NSString *)from to:(NSString *)to
{
    CLGeocoder *coder = [[CLGeocoder alloc]init];
    
    __block CLLocationCoordinate2D startCoordinate;
    __block CLLocationCoordinate2D endCoordinate;
    
    [coder geocodeAddressString:from completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error)
     {
         if (nil == error)
         {
             CLPlacemark *place = [placemarks firstObject];
             startCoordinate = place.location.coordinate;
             
             [coder geocodeAddressString:to completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error)
              {
                  if (nil == error)
                  {
                      CLPlacemark *place = [placemarks firstObject];
                      endCoordinate = place.location.coordinate;
                      
                      [self getDirectionsFrom:startCoordinate to:endCoordinate withCompletionHandler:^(MKDirectionsResponse *response, NSError *error) {
                          if (!error)
                          {
                              [self createArrayFromRoute:response];
                          }
                      }];
                  }
                  else
                  {
                      NSLog(@"%@",error);
                  }
              }];
         }
         else
         {
             NSLog(@"%@",error);
         }
     }];
}

- (void) createArrayFromRoute:(MKDirectionsResponse *)response
{
    points = [[NSMutableArray alloc]init];
    
    MKRoute *route = [response.routes firstObject];
    
    NSUInteger pointCount = route.polyline.pointCount;
    
    //allocate a C array to hold this many points/coordinates...
    CLLocationCoordinate2D *routeCoordinates = malloc(pointCount * sizeof(CLLocationCoordinate2D));
    
    //get the coordinates (all of them)...
    [route.polyline getCoordinates:routeCoordinates range:NSMakeRange(0, pointCount)];
    
    for (int i = 0; i < pointCount; i++)
    {
        @autoreleasepool
        {
            TCoreLocationEntity *entity = [[TCoreLocationEntity alloc]init];
            entity.latitude = routeCoordinates[i].latitude;
            entity.longitude = routeCoordinates[i].longitude;
            entity.horizontalAccuracy = arc4random_uniform(64);
            entity.speed = arc4random_uniform(35) + 10;
            if (i == 0)
            {
                entity.timestamp = [NSDate new];
            }
            else if (i < pointCount - 1)
            {
                TCoreLocationEntity *prev = [points lastObject];
                float distance = fabs([self distanceFrom:entity and:prev]);
                entity.course = [self angleFromCoordinate:CLLocationCoordinate2DMake(entity.latitude, entity.longitude) toCoordinate:CLLocationCoordinate2DMake(prev.latitude, prev.longitude)];
                NSTimeInterval interval = distance / prev.speed;
                entity.timestamp = [[NSDate alloc] initWithTimeInterval:interval sinceDate:prev.timestamp];
                
                if (interval > 2 && points.count > 2)
                {
                    //Need to fill this gap
                    unsigned int gaps = (int)interval;
                    float speedGap    = (entity.speed - prev.speed) / gaps;
                    float courseGap   = (entity.course - prev.course) /gaps;
                    float latGap      = (entity.latitude - prev.latitude) / gaps;
                    float lngGap      = (entity.longitude - prev.longitude) / gaps;
                    float timeGap     = interval / gaps;
                    
                    for (unsigned int i = 0; i < gaps; i++)
                    {
                        @autoreleasepool
                        {
                            TCoreLocationEntity *gapEntity = [[TCoreLocationEntity alloc]init];
                            
                            gapEntity.speed     = prev.speed     + speedGap;
                            gapEntity.course    = prev.course    + courseGap;
                            gapEntity.latitude  = prev.latitude  + latGap;
                            gapEntity.longitude = prev.longitude + lngGap;
                            gapEntity.timestamp = [prev.timestamp dateByAddingTimeInterval:timeGap];
                            gapEntity.horizontalAccuracy = arc4random_uniform(64);
                            gapEntity.gap = YES;
                            [points addObject:gapEntity];
                            prev = [gapEntity copy];
                        }
                    }
                }
            }
            
            [points addObject:entity];
        }
    }
    
    //free the memory used by the C array when done with it...
    free(routeCoordinates);
    
    NSArray *reverse = [[points reverseObjectEnumerator] allObjects];
    points = [[NSMutableArray alloc]initWithArray:reverse];
    
    if(_createRouteHandler) self.createRouteHandler([points copy]);
    
    [_timer startTimer];
}

- (void) getDirectionsFrom:(CLLocationCoordinate2D)sourceCoords to:(CLLocationCoordinate2D)destCoords withCompletionHandler:(void(^)(MKDirectionsResponse *response, NSError *error))completion
{
    
    MKPlacemark *placemark  = [[MKPlacemark alloc] initWithCoordinate:sourceCoords addressDictionary:nil];
    MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
    MKPlacemark *placemark1  = [[MKPlacemark alloc] initWithCoordinate:destCoords addressDictionary:nil];
    MKMapItem *mapItem1 = [[MKMapItem alloc] initWithPlacemark:placemark1];
    
    MKDirectionsRequest *request = [[MKDirectionsRequest alloc] init];
    request.source = mapItem1;
    request.destination = mapItem;
    request.requestsAlternateRoutes = NO;
    
    MKDirections *directions = [[MKDirections alloc] initWithRequest:request];
    [directions calculateDirectionsWithCompletionHandler: ^(MKDirectionsResponse *response, NSError *error)
     {
         completion(response,error);
     }];
}

- (float)angleFromCoordinate:(CLLocationCoordinate2D)first toCoordinate:(CLLocationCoordinate2D)second
{
    float deltaLongitude = second.longitude - first.longitude;
    float deltaLatitude = second.latitude - first.latitude;
    float angle = (M_PI * .5f) - atan(deltaLatitude / deltaLongitude);
    
    if (deltaLongitude > 0)      return angle;
    else if (deltaLongitude < 0) return angle + M_PI;
    else if (deltaLatitude < 0)  return M_PI;
    
    return 0.0f;
}

- (float) distanceFrom:(TCoreLocationEntity *)start and:(TCoreLocationEntity*)end
{
    CLLocation *startLocation = [[CLLocation alloc]initWithLatitude:start.latitude longitude:start.longitude];
    CLLocation   *endLocation = [[CLLocation alloc]initWithLatitude:end.latitude longitude:end.longitude];
    float distance = [startLocation distanceFromLocation:endLocation];
    
    return distance;
}

#pragma maark - Timer delegate

- (void) timerFired:(id)sender
{
    NSInteger idx = [_timer getInstances];
    
    if (idx < points.count)
    {
        TCoreLocationEntity *entity = points[idx];
        entity.timestamp = [NSDate date];
        if(_mockLocation) self.mockLocation(entity,NO);
    }
    else
    {
        if(_mockLocation) self.mockLocation(nil,YES);
        [_timer cancelTimer];
    }
}

#pragma mark - Public methods

- (void) routeHandler:(void (^)(NSArray *route))routeHandler
{
    self.createRouteHandler = routeHandler;
}

- (void) mockLocationHandler:(void(^)(_Nullable id location, BOOL isLast))mockLocationHandler
{
    self.mockLocation = mockLocationHandler;
}

@end
