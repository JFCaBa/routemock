//
//  TrakMockRoute.h
//  RouteMock-ios
//
//  Created by Jose Catala on 04/12/2018.
//  Copyright © 2018 TrakGlobal Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^RouteBlock)(NSArray *);

typedef void (^MockLocationBlock)(_Nullable id, BOOL);

@interface TrakCoreMockRoute : NSObject

@property (nonatomic, copy) RouteBlock createRouteHandler;

@property (nonatomic, copy) MockLocationBlock mockLocation;

- (instancetype) init NS_UNAVAILABLE;
- (instancetype) initFromLocation:(nonnull NSString *)from toLocation:(nonnull NSString *)to NS_DESIGNATED_INITIALIZER;

//TODO: Add an error handler here
- (void) routeHandler:(void (^)(NSArray *route))routeHandler;

- (void) mockLocationHandler:(void(^)(_Nullable id location, BOOL isLast))mockLocationHandler;

@end

NS_ASSUME_NONNULL_END
