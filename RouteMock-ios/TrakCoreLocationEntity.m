//
//  TrakCoreLocationEntity.m
//  TrakCoreFramework
//
//  Created by Jose Catala on 02/11/2018.
//  Copyright © 2018 TrakGlobal Telematics. All rights reserved.
//

#import "TrakCoreLocationEntity.h"
#import <CoreLocation/CoreLocation.h>

#import <objc/runtime.h>

@implementation TCoreLocationEntity

#pragma mark -  Initializers

- (instancetype) init
{
    self = [super init];
    
    if (self)
    {
        
    }
    
    return self;
}

- (instancetype) initWithLocation:(CLLocation * _Nullable)location
{
    if (!location) return nil;
    
    self = [super init];
    
    if (self)
    {
        _latitude           = location.coordinate.latitude;
        _longitude          = location.coordinate.longitude;
        _altitude           = location.altitude;
        _horizontalAccuracy = location.horizontalAccuracy;
        _verticalAccuracy   = location.verticalAccuracy;
        _speed              = location.speed;
        _course             = location.course;
        _timestamp          = location.timestamp;
        _gap                = NO;
    }
    
    return self;
}

- (void)encodeWithCoder:(nonnull NSCoder *)aCoder
{
    [aCoder encodeFloat:self.latitude              forKey:@"latitude"];
    [aCoder encodeFloat:self.longitude             forKey:@"longitude"];
    [aCoder encodeFloat:self.altitude              forKey:@"altitude"];
    [aCoder encodeFloat:self.horizontalAccuracy    forKey:@"horizontalAccuracy"];
    [aCoder encodeFloat:self.verticalAccuracy      forKey:@"verticalAccuracy"];
    [aCoder encodeFloat:self.speed                 forKey:@"speed"];
    [aCoder encodeFloat:self.course                forKey:@"course"];
    [aCoder encodeObject:self.timestamp            forKey:@"timestamp"];
    [aCoder encodeBool:self.gap                    forKey:@"gap"];
}

- (nullable instancetype)initWithCoder:(nonnull NSCoder *)aDecoder
{
    self = [super init];
    
    if (self)
    {
        _latitude           = [aDecoder decodeFloatForKey:@"latitude"];
        _longitude          = [aDecoder decodeFloatForKey:@"longitude"];
        _altitude           = [aDecoder decodeFloatForKey:@"altitude"];
        _horizontalAccuracy = [aDecoder decodeFloatForKey:@"horizontalAccuracy"];
        _verticalAccuracy   = [aDecoder decodeFloatForKey:@"verticalAccuracy"];
        _verticalAccuracy   = [aDecoder decodeFloatForKey:@"verticalAccuracy"];
        _speed              = [aDecoder decodeFloatForKey:@"speed"];
        _course             = [aDecoder decodeFloatForKey:@"course"];
        _timestamp          = [aDecoder decodeObjectForKey:@"timestamp"];
        _gap                = [aDecoder decodeBoolForKey:@"gap"];
    }
    
    return self;
}

- (nonnull id)copyWithZone:(nullable NSZone *)zone
{
    TCoreLocationEntity *copy = [[[self class] allocWithZone:zone] init];
    
    if (copy)
    {
        copy.latitude           = self.latitude;
        copy.longitude          = self.longitude;
        copy.altitude           = self.altitude;
        copy.horizontalAccuracy = self.horizontalAccuracy;
        copy.verticalAccuracy   = self.verticalAccuracy ;
        copy.speed              = self.speed;
        copy.course             = self.course;
        copy.timestamp          = self.timestamp;
        copy.gap                = self.gap;
    }
    
    return copy;
}

#pragma mark - OverWriting method

- (NSString *)description
{
    unsigned int numIvars = 0;
    Ivar * ivars = class_copyIvarList([self class], &numIvars);
    
    NSMutableString *mStr = [[NSMutableString alloc]initWithString:[NSString stringWithFormat:@"*** %@ Description ***\n",NSStringFromClass([self class])]];
    
    for(int i = 0; i < numIvars; i++)
    {
        Ivar thisIvar = ivars[i];
        NSString * classKey = [NSString stringWithUTF8String:ivar_getName(thisIvar)];
        
        NSString *line = [NSString stringWithFormat:@"%@: %@ | ",classKey,[self valueForKey:classKey]];
        
        [mStr appendString:line];
    }
    
    if (numIvars > 0)  free(ivars);
    
    [mStr appendString:@"\n\n"];
    return [mStr copy];
}
@end
