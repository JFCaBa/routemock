//
//  StartVC.h
//  RouteMock-ios
//
//  Created by Jose Catala on 28/11/2018.
//  Copyright © 2018 TrakGlobal Telematics. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface StartVC : UIViewController

@end

NS_ASSUME_NONNULL_END
