//
//  TrakCoreGCDTimer.h
//  TrakCoreFramework
//
//  Created by Jose Catala on 12/11/2018.
//  Copyright © 2018 TrakGlobal Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol TrakCoreGCDTimerDelegate

- (void) timerFired:(id)sender;

@end

@interface TrakCoreGCDTimer : NSObject

@property (nonatomic, weak) id<TrakCoreGCDTimerDelegate>delegate;

/*!
 *@brief Designated initializer passing the tick interval to the Class
 */
- (instancetype) initWithInterval:(NSTimeInterval)interval NS_DESIGNATED_INITIALIZER;

- (instancetype) init NS_UNAVAILABLE;

- (void) startTimer;

- (void) cancelTimer;

/*!
 *@brief    To know how many times the onfire has been triggered according to the interval
 *@return   The number of times the timer has been fired.
 */
- (NSInteger)getInstances;

@end

NS_ASSUME_NONNULL_END
